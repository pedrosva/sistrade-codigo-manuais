import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Content />
      </div>
    );
  }
}

class Header extends Component {
  render() {
    return (
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Welcome to React</h1>
      </header>
    );
  }
}

const red = '#ff0000';
const blue = '#3600ff';

class Content extends Component {
  constructor(props) {
    super(props);
    this.state = { color: blue };
    this.changeColor = this.changeColor.bind(this);
  }

  changeColor() {
    const newMood = this.state.color === blue ? red : blue;
    this.setState({ color: newMood });
  }

  render() {
    return (
      <div>
      <h1 style={{ color: this.state.color }}>
        Change my color
      </h1>
      <button onClick={this.changeColor}>
        Change color
      </button>
    </div>
    );
  }
}

export default App;