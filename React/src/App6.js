import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Content firstName='REACT'/>
      </div>
    );
  }
}
class Header extends Component {
  render() {
    return (
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Welcome to React</h1>
      </header>
    );
  }
}
class Content extends Component {
  render() {
    return <h1>Hi there, {this.props.firstName}!</h1>
  }
}

export default App;