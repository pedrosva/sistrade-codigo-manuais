import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Content />
      </div>
    );
  }
}

class Header extends Component {
  render() {
    return (
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Welcome to React</h1>
      </header>
    );
  }
}

class Content extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: 'Initial data...'
    }
    this.updateState = this.updateState.bind(this);
    this.updateState2 = this.updateState2.bind(this);
  };
  updateState(e) {
    this.setState({ data: e.target.value });
  }
  updateState2(e) {
    this.setState({ data: 'Initial data...' });
  }
  render() {
    return (
      <div className="top">
        <input type="text" value={this.state.data}
          onChange={this.updateState} />
        <h4>{this.state.data}</h4>
        <button onClick = {this.updateState2}>Reset</button>
      </div>
    );
  }
}

export default App;



