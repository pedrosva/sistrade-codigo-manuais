import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {

    const items = ['Home', 'Shop', 'About Me'];
    const listItems = items.map((item, i) =>
      <li key={'item_' + i}>{item}</li>
    );
    
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <ul>{listItems}</ul>
      </div>
    );
  }
}

export default App;