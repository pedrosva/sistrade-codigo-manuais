import { Component, OnInit } from '@angular/core';
import { ProjectsService } from '../projects.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  projectList = [];

  constructor(private project : ProjectsService) { }

  ngOnInit() {

    this.project.getProject().subscribe(result => {
      console.log(result);
      this.projectList=result;
    });

  }
}

