import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
}
)
export class ProjectsService {

  constructor(private http: HttpClient) { }

  getProject() : Observable<any[]> {
    return this.http.get<any[]>("http://localhost:3000/projects");

  }
}

