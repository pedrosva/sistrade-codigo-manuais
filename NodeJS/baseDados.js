var express = require('express');
const bodyParser = require("body-parser");
var app = express();
app.use(require('cors')());
app.use(bodyParser.json());
const sql = require('mssql');
const connStr = "Server=ENDERECO_SERVIDOR;Database=NOME_BD;User Id=USERNAME;Password=PASSWORD;";

sql.connect(connStr)
   .then(conn => global.conn = conn)
   .catch(err => console.log(err));

app.get('/projects', function (req, res) {
   execSQLQuery('select * from dbo.Projects', res);
});

var server = app.listen(3000, function () {
   console.log('Listening at 3000');
})

function execSQLQuery(sqlQry, res){
    global.conn.request()
               .query(sqlQry)
               .then(result => res.json(result.recordset))
               .catch(err => res.json(err));
}
