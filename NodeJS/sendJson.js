var express = require('express');
const bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.json());

var movies = [
    { "title": "Ghostbusters" },
    { "title": "Star Wars" },
    { "title": "Batman Begins" }
  ];

app.get('/', function (req, res) {
   res.send('Hello World');
})

app.get('/movies', function (req, res) {
    res.send(movies);
})
 
var server = app.listen(3000, function () {
   console.log("Example app listening at 3000");
})

